# hazard.py
a curses client for the danger/u/ textboard

## controls
- left and right arrows move back and forward in history
- up and down arrows scroll
- enter to select
- p to create a new thread or reply
- 1-0 keys will go to that respective page of a board (0 = 10)
- / to find a thread reply by id, hash, or text match

## todo
- code cleanup
- windows
- requests error handling
- mouse support (?)
