#!/usr/bin/python3
# hazard.py
# Copyright (C) 2018  oneseveneight
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Email: oneseveneight@airmail.cc

import curses
import curses.textpad
import json
import requests


class NewSection:
    def __init__(self, section_json, drawFunc):
        self.json = section_json
        self.max_sel = -1
        self.scroll = 0
        self.selection = [0, '', '']  # Number in list, id, type
        self.draw = drawFunc


def addStrAligned(string, window, attr, align):
    max_y = window.getmaxyx()[0]
    max_x = window.getmaxyx()[1]

    curs_y = window.getyx()[0]
    curs_x = window.getyx()[1]

    if align == 0:
        x_start_align = 0
    elif align == 1:
        x_start_align = (max_x // 2) - (min(len(string), max_x) // 2)

    window.move(curs_y, curs_x + x_start_align)

    for c in string:
        if curs_y < max_y - 1:
            window.addch(c, attr)
        else:
            return -1

        curs_y = window.getyx()[0]
        curs_x = window.getyx()[1]

    curs_y = window.getyx()[0]

    if curs_y < max_y - 1:
        window.addch('\n')
    else:
        return -1
    return 0


def loadSection(window, name, url, drawFunc):
    window.clear()
    window.addstr('Loading ' + name + '...')
    window.refresh()
    Section = NewSection(json.loads(requests.get(url).text), drawFunc)
    Section.draw(window, Section)
    return Section


def newPost(window, info_text, max_line):
    curses.curs_set(True)

    if max_line is None:
        max_y = window.getmaxyx()[0]
    else:
        max_y = max_line + 2

    max_x = window.getmaxyx()[1]

    window.clear()
    window.refresh()

    window.addstr(info_text + ' (Ctrl-g to finish): \n')
    window.refresh()

    text_win = curses.newwin(max_y - 2, max_x, 1, 0)
    TextBox = curses.textpad.Textbox(text_win)
    post_text = TextBox.edit()

    window.move(max_y - 1, 0)
    window.addstr('Submit? (Y/N)')
    window.refresh()
    curses.curs_set(False)
    while True:
        ch = window.getch()
        if ch == ord('y') or ch == ord('Y'):
            return post_text
        elif ch == ord('n') or ch == ord('N'):
            return None


def drawReplyList(window, ReplyList):
    window.clear()

    addStrAligned(ReplyList.json[0]['title'], window, curses.A_BOLD, 0)

    for i in range(ReplyList.scroll, len(ReplyList.json)):
        reply_text = ReplyList.json[i]['comment']
        reply_text = reply_text.replace('\r', '')
        reply_text = '| ' + reply_text

        reply_info_str = ('Post #'
                          + str(ReplyList.json[i]['post_id'])
                          + ', ID, '
                          + ReplyList.json[i]['hash'])

        addStrAligned('\n' + reply_info_str, window, 0, 0)

        if ReplyList.selection[0] == i:
            if addStrAligned(reply_text, window, curses.A_STANDOUT, 0) != 0:
                ReplyList.selection[0] -= 1
                return -1

            ReplyList.selection[1] = (ReplyList.json[i]['post_id'],
                                      ReplyList.json[i]['hash'])
            ReplyList.selection[2] = 'reply'

        elif addStrAligned(reply_text, window, 0, 0) != 0:
            ReplyList.max_sel = i - 1
            return -1

    window.refresh()


def drawThreadList(window, ThreadList):
    window.clear()

    max_y = window.getmaxyx()[0]
    if max_y > len(ThreadList.json) + 3:
        window.move((max_y // 2) - (len(ThreadList.json) // 2) - 2, 0)
        addStrAligned('/' + ThreadList.json[0]['board'] + '/',
                      window,
                      curses.A_BOLD, 1)
        window.move((max_y // 2) - (len(ThreadList.json) // 2), 0)

    for i in range(ThreadList.scroll, len(ThreadList.json)):
        if ThreadList.json[i]['is_locked']:
            thread_status = '(Locked)'
        else:
            thread_status = ''

        thread_title = (ThreadList.json[i]['title']
                        + ' - '
                        + str(ThreadList.json[i]['number_of_replies'])
                        + ' ' + thread_status)

        if i == ThreadList.selection[0]:
            if addStrAligned(thread_title, window, curses.A_STANDOUT, 1) != 0:
                return -1

            ThreadList.selection[1] = ThreadList.json[i]['post_id']
            ThreadList.selection[2] = 'thread'

        elif addStrAligned(thread_title, window, 0, 1) != 0:
            ThreadList.max_sel = i - 1
            return -1

    window.refresh()


def drawBoardList(window, BoardList):
    window.clear()
    max_y = window.getmaxyx()[0]
    if max_y > len(BoardList.json):
        window.move((max_y // 2) - (len(BoardList.json) // 2), 0)
    for i in range(BoardList.scroll, len(BoardList.json)):
        if i == BoardList.selection[0]:
            if addStrAligned(BoardList.json[i],
                             window,
                             curses.A_STANDOUT, 1) != 0:
                return -1

            BoardList.selection[1] = BoardList.json[i]
            BoardList.selection[2] = 'board'

        elif addStrAligned(BoardList.json[i], window, 0, 1) != 0:
            BoardList.max_sel = i - 1
            return -1

    window.refresh()


def main(stdscr):
    curses.start_color()
    curses.use_default_colors()
    curses.curs_set(False)
    curses.qiflush(True)
    stdscr.timeout(0)
    stdscr.idlok(True)
    stdscr.scrollok(False)
    stdscr.nodelay(False)

    stdscr.clear()
    stdscr.addstr('Loading boards...')
    stdscr.refresh()

    url = 'https://dangeru.us/api/v2/boards'

    BoardList = NewSection(json.loads(requests.get(url).text),
                           drawBoardList)
    section_history = []

    CurrentSection = BoardList
    CurrentSection.draw = drawBoardList
    CurrentSection.draw(stdscr, CurrentSection)

    section_history.append(CurrentSection)
    history_loc = len(section_history) - 1

    while True:
        ch = stdscr.getch()

        if ch == ord('q'):
            break

        elif ch == curses.KEY_UP:
            curses.flushinp()

            if CurrentSection.selection[0] != 0:
                CurrentSection.selection[0] -= 1
                if CurrentSection.selection[0] == CurrentSection.scroll - 1:
                    CurrentSection.scroll -= 1

            CurrentSection.draw(stdscr, CurrentSection)

        elif ch == curses.KEY_DOWN:
            curses.flushinp()

            if CurrentSection.selection[0] < len(CurrentSection.json) - 1:
                if CurrentSection.selection[0] == CurrentSection.max_sel:
                    CurrentSection.scroll += 1

                CurrentSection.selection[0] += 1

            CurrentSection.draw(stdscr, CurrentSection)

        elif ch == curses.KEY_LEFT:
            curses.flushinp()

            if history_loc != 0:
                history_loc -= 1

            CurrentSection = section_history[history_loc]
            CurrentSection.draw(stdscr, CurrentSection)

        elif ch == curses.KEY_RIGHT:
            curses.flushinp()

            if history_loc != len(section_history) - 1:
                history_loc += 1

            CurrentSection = section_history[history_loc]
            CurrentSection.draw(stdscr, CurrentSection)

        elif ch == ord('p'):
            if (CurrentSection.selection[2] == 'reply'
               and not CurrentSection.json[0]['is_locked']):
                board, parent, content = '', '', ''

                for Section in section_history:
                    if Section.selection[2] == 'board':
                        board = Section.selection[1]
                    elif Section.selection[2] == 'thread':
                        parent = Section.selection[1]
                        content = newPost(stdscr, 'Repling to thread #'
                                                  + str(parent), None)
                        break

                if content is not None:
                    url = 'https://dangeru.us/reply'

                    post_data = {'board': board,
                                 'parent': parent,
                                 'content': content}

                    requests.post(url, data=post_data)

                CurrentSection.draw(stdscr, CurrentSection)

            elif CurrentSection.json[0]['is_locked']:
                stdscr.addstr(stdscr.getmaxyx()[0] - 1, 0, 'Post is locked.')
                stdscr.refresh()

            elif CurrentSection.selection[2] == 'thread':
                board, title, comment = '', '', ''

                for Section in section_history:
                    if Section.selection[2] == 'board':
                        board = Section.selection[1]

                title = newPost(stdscr, 'Title for new thread', 1)
                comment = newPost(stdscr, 'Comment for new thread', None)

                if title is not None:
                    url = 'https://dangeru.us/post'

                    post_data = {'board': board,
                                 'title': title.replace('\n', ''),
                                 'comment': comment}

                    requests.post(url, data=post_data)

                CurrentSection.draw(stdscr, CurrentSection)

        elif (ch == ord('/')
              and CurrentSection.selection[2] == 'reply'):
            prev_json = CurrentSection.json
            prev_scroll = CurrentSection.scroll

            query = ''
            matches = []
            stdscr.addch(stdscr.getmaxyx()[0] - 1, 0, '/')

            while True:
                max_y = stdscr.getmaxyx()[0]
                ch = stdscr.getch()

                if ch >= 32 and ch <= 122:
                    query = query + chr(ch)

                elif (ch == curses.KEY_ENTER or ch == 10
                      or ch == curses.KEY_RESIZE):
                    stdscr.clear()
                    stdscr.refresh()
                    CurrentSection.json = prev_json
                    CurrentSection.draw(stdscr, CurrentSection)
                    break

                elif (ch == curses.KEY_BACKSPACE or ch == 127):
                    query = query[:-1]

                matches = [reply_json for reply_json
                           in prev_json[1:]
                           if query in str(reply_json['post_id'])
                           or query in reply_json['hash']
                           or query in reply_json['comment']]

                matches.insert(0, CurrentSection.json[0])

                CurrentSection.json = matches
                CurrentSection.scroll = 0
                CurrentSection.draw(stdscr, CurrentSection)
                stdscr.addch(max_y - 1, 0, '/')
                stdscr.addstr(max_y - 1, 1, query)

            CurrentSection.scroll = prev_scroll

        elif (ch == curses.KEY_ENTER or ch == 10
              or ch == ord('r')
              or ch - 48 in range(10)
              and CurrentSection.selection[2] == 'thread'):
            curses.flushinp()

            page_num = ''

            if ch == ord('r'):
                if CurrentSection.selection[2] == 'board':
                    CurrentSection.selection[2] = 'none'
                else:
                    CurrentSection = section_history[history_loc - 1]
                    section_history = section_history[:history_loc]

            elif ch - 48 in range(10):
                if ch - 48 == 0:
                    ch += 10

                CurrentSection.selection[2] = 'board'
                CurrentSection.selection[1] = CurrentSection.json[0]['board']
                page_num = '?page=' + str(ch - 49)

            if CurrentSection.selection[2] == 'board':
                url = ('https://dangeru.us/api/v2/board/'
                       + str(CurrentSection.selection[1])
                       + page_num)

                CurrentSection = loadSection(stdscr,
                                             'threads',
                                             url,
                                             drawThreadList)

            elif CurrentSection.selection[2] == 'thread':
                url = ('https://dangeru.us/api/v2/thread/'
                       + str(CurrentSection.selection[1])
                       + '/replies')

                CurrentSection = loadSection(stdscr,
                                             'replies',
                                             url,
                                             drawReplyList)

            elif CurrentSection.selection[2] == 'reply':
                selected_reply_id = str(CurrentSection.selection[1][0])
                selected_reply_hash = str(CurrentSection.selection[1][1])

                for Section in section_history:
                    if Section.selection[2] == 'reply':
                        CurrentSection = Section
                        break

                CurrentSection = NewSection(CurrentSection.json, drawReplyList)

                CurrentSection.json = [reply_json for reply_json
                                       in CurrentSection.json[1:]
                                       if '>>' + selected_reply_id
                                       in reply_json['comment']
                                       or '>>' + selected_reply_hash
                                       in reply_json['comment']]

                if len(CurrentSection.json) != 0:
                    CurrentSection.json[0]['title'] = ('Replies to #'
                                                       + selected_reply_id
                                                       + ', '
                                                       + selected_reply_hash)
                else:
                    CurrentSection = section_history[history_loc]
                    section_history = section_history[:history_loc]

            elif CurrentSection.selection[2] == 'none':
                stdscr.clear()
                stdscr.addstr('Loading boards...')
                stdscr.refresh()

                url = 'https://dangeru.us/api/v2/boards'

                CurrentSection = NewSection(json.loads(requests.get(url).text),
                                            drawBoardList)
                section_history = section_history[:history_loc]

            section_history = section_history[:history_loc + 1]
            section_history.append(CurrentSection)
            history_loc = len(section_history) - 1
            CurrentSection.draw(stdscr, CurrentSection)

        elif ch == curses.KEY_RESIZE:
            CurrentSection.draw(stdscr, CurrentSection)


curses.wrapper(main)
